import Vue from 'vue'
import App from './App.vue'

export const eventBus = new Vue(); // event bus needs to be loaded before Vue instance

new Vue({
  el: '#app',
  render: h => h(App)
})