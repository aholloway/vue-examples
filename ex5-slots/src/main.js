import Vue from 'vue'
import App from './App.vue'

export const eventBus = new Vue({
  methods: {
    changeLocation(location) {
      this.$emit('locationWasEdited', location);
    }
  }
});

new Vue({
  el: '#app',
  render: h => h(App)
})